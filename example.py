from htmlparser import Parser


conf = "parser.conf"

url = "https://lenta.ru/news/2018/05/17/stars/"
p1 = Parser.configure(url, conf)

url = "https://lenta.ru/news/2018/05/22/roscos/"
p2 = Parser.configure(url, conf)

url = "https://lenta.ru/news/2018/05/24/sbre_dron/"
p3 = Parser.configure(url, conf)

url = "https://www.gazeta.ru/tech/2018/05/20/11755531/andro_functions.shtml"
p4 = Parser.configure(url, conf)

url = "https://www.gazeta.ru/tech/2018/03/25/11693287/gray_cardinals.shtml"
p5 = Parser.configure(url, conf)

p1.parse_and_save(save_root_dir="./result-example")
#p1.parse()
#print(p1.parsed_text)
#p1.save(root_dir="./result-example")

p2.parse_and_save(save_root_dir="./result-example")
#p2.parse()
#print(p2.parsed_text)
#p2.save(root_dir="./result-example")

p3.parse_and_save(save_root_dir="./result-example")
#p3.parse()
#print(p3.parsed_text)
#p3.save(root_dir="./result-example")

p4.parse_and_save(save_root_dir="./result-example")
#p4.parse()
#print(p4.parsed_text)
#p4.save(root_dir="./result-example")

p5.parse_and_save(save_root_dir="./result-example")
