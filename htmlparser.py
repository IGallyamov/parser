import re
from urllib.request import urlopen, URLError
from urllib.parse import urlsplit
import os


class Parser():
    """ Извлекает полезный контент сайта и печатает в файл.
        content_blocks - шаблон, который задает поиск полезного контента.
        ignore_blocks - шаблон, который игнорирует нежелательный контент.
        См. файл конфигурации.

        Пользовательские методы:
        Parser.configure(cls, url, conf)
        parse_and_save(self, save_root_dir)
        parse_and_read(self)
        parse(self)
        save(self, root_dir="./data")
        self.parsed_text
    """

    def __init__(self, url, content_blocks, ignore_blocks="",
                 encoding="utf-8", max_column_width=80):

        self.url = url
        if not self.url:
            raise ValueError("url не может быть пустым.")

        if max_column_width < 10:
            raise ValueError(
                "max_column_width не должно быть меньше 10")

        self.content_block_list = self._compile_pattern_list(
            content_blocks, sep="&")
        if not self.content_block_list:
            raise ValueError(
                "content_block_list должен содержать хотя бы один блок.")

        _ignore = "#script|#style|{0}".format(ignore_blocks)
        self.ignore_block_list = self._compile_pattern_list(_ignore, sep="|")

        self.encoding = encoding
        self.max_column_width = max_column_width
        self.parsed_text = None

    @classmethod
    def configure(cls, url, conf):
        """ Создает экземпляр класса Parser на основе конфиг-файла. """

        def read_config():
            """ Возвращает список всех настроек из файла. """

            conf_list = []
            conf_dict = {}
            with open(conf, 'r') as f:
                for line in f:
                    line = line.strip()
                    if not line or line.startswith("#"):
                        continue
                    r = re.match("^\[(.+)\]$", line)
                    if r:
                        if conf_dict:
                            conf_list.append(conf_dict)
                            conf_dict = {}
                        conf_dict["name"] = r.group(1).strip()
                    else:
                        k, v = line.split("=", 1)
                        conf_dict[k.strip()] = v.strip()

            if conf_dict:
                conf_list.append(conf_dict)
            return conf_list

        # Ищем конфигурацию для текущего сайта
        configs = read_config()

        url_info = urlsplit(url)
        if hasattr(url_info, 'netloc') and url_info.netloc:
            conf = [x for x in configs if x["name"] == url_info.netloc]
            if conf:
                conf = conf[0]
            else:
                raise Exception(
                    "Не найдена конфигурация для %s" % url_info.netloc)
        else:
            raise Exception("Не могу разобарть url %s" % url)

        attrs = (
            ("url", url),
            ("content_blocks", conf.get("content_blocks", [])),
            ("ignore_blocks", conf.get("ignore_blocks", None)),
            ("encoding", conf.get("encoding", None)),
            ("max_column_width", conf.get("max_column_width", None)),
        )

        _attrs = dict(filter(lambda x: x[1] is not None, attrs))

        if "max_column_width" in _attrs:
            _attrs["max_column_width"] = int(_attrs["max_column_width"])

        return cls(**_attrs)

    def parse_and_save(self, save_root_dir):
        self.parse()
        self.save(save_root_dir)

    def parse_and_read(self):
        self.parse()
        print(self.parsed_text)

    def parse(self):
        """ Записывает в self.parsed_text найденный текст. """

        html = self._get_html()
        html_content = self._get_html_content(html)
        html_data = self._recursive_extract_content(html_content)
        self.parsed_text = self._builder_text(html_data)
        print("Ok. Получено {0} символов.".format(len(self.parsed_text)))
        return True

    def save(self, root_dir="./data"):
        """ Сохраняет текст с помощью класса SaveManager. """

        if not root_dir:
            print ("root-директория не задана.")
            return False

        if not self.parsed_text:
            print ("Текст не получен. Нечего сохранять.")
            return False

        save_text = self.SaveManager(
            url=self.url, text=self.parsed_text, root_dir=root_dir)
        save_text.create_dir()
        res = save_text.save()
        if not isinstance(res, Exception):
            print ("Текст сохранен в папку {0}\n".format(res))
        else:
            print ("Не удалось сохранить текст. Error {0}. ".format(res))
        return True

    def _compile_pattern_list(self, param, sep):
        """ Возвращает лист паттернов """

        if not sep:
            raise ValueError("Разделить не может быть пустым.")

        if not param:
            return []

        p = [x.strip() for x in param.split(sep) if x]
        return [self._compile_pattern(x)[0] for x in p]

    def _compile_pattern(self, block):
        """ Возвращает (pattern, attrs) для поиска контента """

        if not block:
            raise CompilePatternError("content block не может быть пустым")

        s = block

        # tag name
        r = re.search(r"^#(\w+)", s)
        if r:
            tag_name = r.group(1)
        else:
            raise CompilePatternError(
                "Не удалось вычислить tag_name для %s" % s)

        # attrs
        attrs = [re.sub("(=)(.*)", "\s?\\1\s?\'?\"?\\2\'?\"?", _)
                 for _ in re.findall(r"\[(.+?)\]", s)]
        attrs_raw = re.findall(r"\[(.+?)\]", s)

        start = "<{0}(?:[^>]*)".format(tag_name)
        pattern = "\s".join([start] + attrs)
        return (pattern, {"tag_name": tag_name, "attrs": attrs_raw})

    def _builder_text(self, data):
        """ Создает текст по data. Затем форматирует его.
            data == [{"value":, "tag":, "attrs":, "parent_tag":, "next_child_tag":},...].
        """

        H_TAGS = ["h%d" % x for x in range(1, 6+1)]
        BLOCK_TAGS = ['div', 'p', 'table', 'time', 'article', ]
        SINGLE_TAGS = ['ul', 'br', ]
        BLOCK_TAGS.extend(H_TAGS)
        BLOCK_TAGS.extend(SINGLE_TAGS)

        def recursive_format_by_width(text, max_width=self.max_column_width):
            """ Форматирует текст по ширине. """

            text_list = []
            idx = max_width - 1
            if len(text) < max_width:
                if text:
                    text_list.append(text)
                return text_list

            r = re.search("(.*?\n+)", text[:max_width])
            if r:
                _ = r.group(1)
                text_list.append(_)
                text = text.replace(_, "", 1).strip()

            elif text[idx] == " " or text[idx] == "\n":
                text_list.append(text[:idx])
                text = text[idx+1:]

            else:  # попали в слово
                _text = text[:idx+1]
                r = re.match("({}[^\s\n]+)".format(re.escape(_text)), text)
                if r:  # попали не на последний символ слова
                    _ = r.group(1)
                    last_word = _.split(" ")[-1]
                    r1 = re.findall(r"(.*)\s{0}$".format(re.escape(last_word)), _)
                    if r1 and r1[0]: # текст до слова
                        text_list.append(r1[0])
                        text = text.replace(r1[0], "", 1).strip()
                    else:  # текст до слова включительно
                        text_list.append(_)
                        text = text.replace(_, "", 1).strip()
                else:  # попали на последний символ слова
                    text_list.append(text[:idx+1])
                    text = text.replace(text[:idx+1], "", 1)

            text_list += recursive_format_by_width(text.strip())
            return text_list

        text = ""
        for x in data:

            if x["value"] == '' and x["tag"] not in SINGLE_TAGS:
                continue

            if x["tag"] == 'br':
                text += "\n"
                continue

            if x["tag"] == 'a':
                r = re.findall(r'href=[\'"]?([^\'"\s>]+)', x["attrs"])
                if not r:
                    text += " {} ".format(x["value"])
                else:
                    text += " {text}[{link}] ".format(text=x["value"], link=r[0])
                continue    

            if x["tag"] in BLOCK_TAGS:
                if x["next_child_tag"] and x["next_child_tag"] not in BLOCK_TAGS:
                    text += x["value"]
                elif x["next_child_tag"]:
                    text += "{}\n".format(x["value"])
                else:
                    text += "{}\n\n".format(x["value"])

            if x["tag"] not in BLOCK_TAGS and x["tag"] != 'a':
                text += "{} ".format(x["value"])

        # форматирование и обработка
        text_list = recursive_format_by_width(text)
        text = "\n".join(text_list)
        text = re.sub(r"^\n*", "", text)
        text = re.sub(r"\n{2,}", "\n\n", text)
        text = "Источник [{url}]\n\n{text}".format(url=self.url, text=text)
        return text

    def _recursive_extract_content(self, html, parent_tag=None):
        """
           Рекурсивно разбирает код.
           Возвращает список словарей вида
           {"value":, "tag":, "attrs":, "parent_tag", "next_child_tag"}.

           <div>T1<p class="clazz" attrs>T2</p>T3<p>T4</p><img src="/path/name" />T5</div> ->
           [
               {'tag': 'div', 'value': 'T1', 'attrs': '', 'parent_tag': None, 'next_child_tag': 'p'},
               {'tag': 'p', 'value': 'T2', 'attrs': 'class="clazz" attrs', 'parent_tag': 'div', 'next_child_tag': None},
               {'tag': 'div', 'value': 'T3', 'attrs': '', 'parent_tag': None, 'next_child_tag': 'p'},
               {'tag': 'p', 'value': 'T4', 'attrs': '', 'parent_tag': None, 'next_child_tag': None},
               {'tag': 'img', 'value': '', 'attrs': 'src="/path/name"', 'parent_tag': None, 'next_child_tag': None},
               {'tag': 'div', 'value': 'T5', 'attrs': '', 'parent_tag': None, 'next_child_tag': None},
           ]
        """

        parse_list = []
        tag, attrs, inner = Parser._get_inner(html)
        tag = tag.lower()

        if not inner:
            parse_dict = {
                "value": '',
                "tag": tag,
                "attrs": attrs,
                "next_child_tag": None,
                "parent_tag": parent_tag
            }
            parse_list.append(parse_dict)

        while len(inner) > 0:
            text = re.match("(.*?)(<|$)", inner).group(1)
            parse_dict = {
                "value": text,
                "tag": tag,
                "attrs": attrs,
                "next_child_tag": None,
                "parent_tag": parent_tag
            }
            parse_list.append(parse_dict)

            inner = inner.replace(text, "", 1)
            if not inner:
                break

            block = Parser._find_block(inner)

            next_child_tag = re.findall("^<(\w+)", block)
            parse_list[-1]["next_child_tag"] = next_child_tag[0]

            parse_list += self._recursive_extract_content(block, parent_tag=tag)
            inner = inner.replace(block, "", 1)
            if not inner:
                break

        return parse_list

    def _get_html_content(self, html):
        """ Ищет заданные блоки контента в html.
            Найденные блоки оборачивает в единый блок.
            и возвращает его как строку.
        """

        def clear(_html, ignore_list):
            # Очищаем от игнорируемых блоков

            for ignore_pattern in ignore_list:
                stop = False
                while not stop:
                    stop = True
                    r = re.search(ignore_pattern, _html)
                    if r:
                        _html_ignore = _html[r.start():]
                        _html_ignore = Parser._find_block(_html_ignore)
                        _html = re.sub(re.escape(_html_ignore), "", _html)
                        stop = False
            return _html

        html = Parser._normalize_html(html)
        pattern_list = self.content_block_list
        pattern_list_ignore = self.ignore_block_list
        _html_list = []
        pattern_count = len(pattern_list)

        for pattern in pattern_list:
            pattern_count -= 1
            r = re.search(pattern, html)
            if not r:
                if pattern_count != 0:
                    msg = "Не могу найти контент по паттерну {0}.".format(pattern)
                    print("Warning: {0}".format(msg))
                    continue
                else:
                    msg = "Контент по заданным параметрам не найден"
                    raise GetContentByPatternError(msg)

            _html = html[r.start():]
            _html = Parser._find_block(_html)
            _html = clear(_html, pattern_list_ignore)
            _html = Parser._normalize_html(_html)
            _html_list.append(Parser._normalize_html(_html))

        _html_list = list(map(lambda x: "<br/>{}".format(x), _html_list))
        return "".join(["<div>"] + _html_list + ["</div>"])

    def __str__(self):
        args = (
            self.url,
            "[\n{}\n]".format("\n".join(self.content_block_list)),
            "[\n{}\n]".format("\n".join(self.ignore_block_list)),
            self.encoding,
            self.max_column_width,
        )

        return ("url={}\n"
                "content_block_list={}\n"
                "ignore_blocks={}\n"
                "encoding={}\n"
                "max_column_width={}\n").format(*args)

    @staticmethod
    def _find_block(block):
        """ Возвращает блок кода между 1-ым найденным тегом
            и соответсвующим ему закрывающим тегом.

            outer_text<div class="clazz"><p>text</p></div><p>other</p> ->
            <div class="clazz"><p>text</p></div>
        """

        SINGLE_TAG = ["img", "br", "hr", ]

        def is_first_tag_single(block):
            idx = block.find(">")
            return "/>" in block[:idx+1]

        start_idx = block.find("<")
        if start_idx == -1:
            raise NotFoundStartTagError(
                "Не могу найти открывающий тег в блоке")
        _block = block[start_idx:]
        tag_name = re.search("^<(\w+)", _block).group(1)

        # одиночный тег, например <img>
        if tag_name in SINGLE_TAG:
            return re.search("^<.+?>", _block).group(0)

        # теги типа <tag_name/>
        if is_first_tag_single(_block):
            return re.search("^<.+?/>", _block).group(0)

        tag_open = "<{0}".format(tag_name)
        tag_close = "</{0}>".format(tag_name)

        idx = 0
        close_tag_found = False
        while idx + 1 < len(_block):
            find_idx = _block[idx+1:].find(tag_close)
            if find_idx == -1:
                break
            idx = find_idx + idx + 1
            _inner_slice = _block[1:idx]
            count_tag_open = _inner_slice.count(tag_open)
            count_tag_close = _inner_slice.count(tag_close)
            if count_tag_open == count_tag_close:
                close_tag_found = True
                break

        if not close_tag_found:
            raise NotFoundCloseTagError(
                "Не удалось найти закрывающий тег для %s" % tag_open)

        return _block[:idx+len(tag_close)]

    @staticmethod
    def _get_inner(block):
        """ Возвращает кортеж (tag, attrs, inner)

            <div class="clazz" attrs><p>text</p></div> ->
            -> (div, class="clazz" attrs, <p>text</p>)
        """

        r = re.findall(r"^<(\w+)(.*?)>(.*?)</\1>$", block)
        if r:
            return (r[0][0], r[0][1].strip(), r[0][2].strip())
        else:  # single tag
            r = re.findall(r"^<(\w+)(.*?)/?>$", block)
            if r:
                return (r[0][0], r[0][1].strip(), "")

        return (None, None, None)

    @staticmethod
    def _normalize_html(html):
        """" Очищает html от лишних символов, т.к. \n \t, пробелы и тд.
             Возвращает нормированный html.
        """

        _html = html

        # убираем лишние символы
        _html = re.sub(r"\n|\t", "", _html)

        # убираем комменты
        _html = re.sub(r"(<!--(?:.*?)-->)", "", _html)

        # замена спец. символов
        _html = re.sub(r"\&laquo;", "«", _html)
        _html = re.sub(r"\&raquo;", "»", _html)
        _html = re.sub(r"\&mdash;", "---", _html)

        # убираем пробелы вокруг '='
        _html = re.sub(r"\s=\s", "=", _html)

        # 2 и более пробела заменяем на 1 пробел
        _html = re.sub(r"\s{2,}|&nbsp;", " ", _html)

        # убираем лишние пробелы в начале и в конце тегов
        _html = re.sub("(<)\s+|\s+(>)|(/)\s+", r"\1\2\3", _html)

        return _html

    @staticmethod
    def _clear_html(html):
        """ Очищает тэги от ненужной информации.
            <div class="clazz">text</div> -> <div>text</div>
        """

        html = Parser._normalize_html(html)
        return re.sub(r"(<\w+).*?(>)", r"\1\2", html)

    def _get_html(self):
        """ Возвращает полную html-страницу """

        try:
            with urlopen(self.url) as response:
                try:
                    html_bytes = response.read()
                    html = html_bytes.decode(self.encoding)
                except LookupError as e:
                    print((
                        "Неизвестная кодировка {enc}. "
                        "Продолжится с кодировкой utf-8.")\
                          .format(enc=self.encoding))
                    self.encoding = "utf-8"
                    html = html_bytes.decode(self.encoding)
        except URLError as e:
            raise RuntimeError("Недоступный ресурс. %s" % e)
        return html

    class SaveManager():
        """ Создает необходимые директории и сохраняет на диск """

        def __init__(self, url, text, root_dir):
            self.url = url
            self.text = text
            self.root_dir = root_dir
            self.full_file_name = None

        def create_dir(self):
            """ Создает директорию на основе url.
                Вычисляет self.full_file_name.
            """

            root = self.root_dir
            file_name = "index.txt"  # может быть переопределено ниже

            url_info = urlsplit(self.url)
            if url_info.path[0] == "/":
                _p = url_info.path[1:]
            else:
                _p = url_info.path

            path = os.path.join(root, url_info.netloc, _p)
            r = re.search("(.+?)(\w+)\.\w+$", path)
            if r:
                # url заканчивается на файл.
                # например, на index.html.
                # то берем путь до файла
                path = r.group(1)
                file_name = "{0}.txt".format(r.group(2))

            try:
                os.makedirs(path)
            except FileExistsError:
                pass

            self.full_file_name = os.path.join(path, file_name)

        def save(self):
            """ Сохраняет текст.
                Возвращает полный путь до файла или ошибку.
            """

            try:
                with open(self.full_file_name, "w") as f:
                    f.write(self.text)
            except Exception as e:
                return e
            return self.full_file_name

    class GetContentByPatternError(Exception):
        pass

    class NotFoundCloseTagError(Exception):
        pass

    class NotFoundStartTagError(Exception):
        pass

    class CompilePatternError(Exception):
        pass


if __name__ == '__main__':
    import sys

    if len(sys.argv) < 3:
        print(("Недостаточно аргументов. "
               "Формат: 'command url --config=configfile'"))
        sys.exit(1)

    args = sys.argv[1:]

    url = args[0]
    _conf = args[1].split("=")
    readonly = False
    try:
        _readonly = args[2]
        if _readonly == "--readonly":
            readonly = True
    except Exception:
        pass

    if len(_conf) < 2:
        print(("Второй аргумент задан неверно. "
               "Формат: 'command url --config=configfile'"))
        sys.exit(1)

    conf = _conf[1]

    p = Parser.configure(url=url, conf=conf)

    if readonly:
        p.parse_and_read()
    else:
        p.parse_and_save("./result-example")
