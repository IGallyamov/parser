import unittest
from htmlparser import Parser


parser = Parser(url="http://phantom", content_blocks="#phantom")


class InitTestCase(unittest.TestCase):
    def test_init_url_none(self):
        with self.assertRaises(ValueError):
            Parser(url=None, content_blocks="#div")

    def test_init_content_block_list_none(self):
        with self.assertRaises(ValueError):
            Parser(url="http://", content_blocks=None)
            
    def test_init_content_block_list_empty(self):
        with self.assertRaises(ValueError):
            Parser(url="http://", content_blocks="")

    def test_max_column_width_small(self):
        with self.assertRaises(ValueError):
            Parser(url="http://", content_blocks="", max_column_width=8)
            
            
class GetHtmlTestCase(unittest.TestCase):
    def test_get_html_url_incorrect(self):
        with self.assertRaises(RuntimeError):
            Parser(url="http://inccorect", content_blocks="#div")._get_html()


class NormalizeHtmlTestCase(unittest.TestCase):

    def test_normalize_html(self):
        html = "< div     class='clazz1    clazz2'  ><   p > text   < / p ><   / div    >"
        standart = "<div class='clazz1 clazz2'><p> text </p></div>"
        _html = Parser._normalize_html(html)
        self.assertEqual(_html, standart)


class ClearHtmlTestCase(unittest.TestCase):

    def test_clear_html(self):
        html = "<div class='clazz' attr=100>text</div>"
        _html = Parser._clear_html(html)
        self.assertEqual(_html, "<div>text</div>")


class GetInnerBlockTestCase(unittest.TestCase):

    def test_get_inner(self):
        html = '<div class="clazz" attrs><p>text</p></div>'
        _html = Parser._get_inner(html)
        self.assertEqual(_html, ('div', 'class="clazz" attrs', '<p>text</p>'))

    def test_get_inner__single_tag(self):
        html = '<img class="clazz" attrs />'
        _html = Parser._get_inner(html)
        self.assertEqual(_html, ('img', 'class="clazz" attrs', ''))

        
class FindHtmlBlockTestCase(unittest.TestCase):
    
    def test_find_block(self):
        html = "outer_text<div class='clazz'><p>text</p></div><p>other</p>"
        standart = "<div class='clazz'><p>text</p></div>"
        _html = Parser._find_block(html)
        self.assertEqual(_html, standart)

    def test_find_block__single_tag(self):
        html = "outer_text<img src='/path/name' /><p>text</p>"
        standart = "<img src='/path/name' />"
        _html = Parser._find_block(html)
        self.assertEqual(_html, standart)
        

class RecursiveExtractConentTestCase(unittest.TestCase):
    
    def test_recursive_extract_content(self):
        html = "<div>T1<p class='clazz' attrs>T2</p>T3<p>T4</p><img src='/path/name' />T5</div>"
        standart = [
            {'value': 'T1', 'tag': 'div', 'attrs': '', 'next_child_tag': 'p', 'parent_tag': None},
            {'value': 'T2', 'tag': 'p', 'attrs': "class='clazz' attrs", 'next_child_tag': None, 'parent_tag': 'div'},
            {'value': 'T3', 'tag': 'div', 'attrs': '', 'next_child_tag': 'p', 'parent_tag': None},
            {'value': 'T4', 'tag': 'p', 'attrs': '', 'next_child_tag': None, 'parent_tag': 'div'},
            {'value': '', 'tag': 'div', 'attrs': '', 'next_child_tag': 'img', 'parent_tag': None},
            {'value': '', 'tag': 'img', 'attrs': "src='/path/name'", 'next_child_tag': None, 'parent_tag': 'div'},
            {'value': 'T5', 'tag': 'div', 'attrs': '', 'next_child_tag': None, 'parent_tag': None}
        ]

        _data = Parser(url='http://site/', content_blocks="#div")._recursive_extract_content(html)
        self.assertEqual(_data, standart)

    def test_recursive_extract_content__nesting(self):
        html = "<div><p class='clazz'><h1 attrs>test</h1></p></div>"
        standart = [
            {'value': '', 'tag': 'div', 'attrs': '', 'next_child_tag': 'p', 'parent_tag': None},
            {'value': '', 'tag': 'p', 'attrs': "class='clazz'", 'next_child_tag': 'h1', 'parent_tag': 'div'},
            {'value': 'test', 'tag': 'h1', 'attrs': 'attrs', 'next_child_tag': None, 'parent_tag': 'p'},
        ]
        
        _data = Parser(url='http://site/', content_blocks="#div")._recursive_extract_content(html)
        self.assertEqual(_data, standart)


class CompilePatternTestCase(unittest.TestCase):
    
    def test_compile_pattern(self):
        block = "#div[class=b-topic__content]"
        res = ('<div(?:[^>]*)\sclass\s?=\s?\'?"?b-topic__content\'?"?', {'tag_name': 'div', 'attrs': ['class=b-topic__content']})
        _block = parser._compile_pattern(block)
        self.assertEqual(_block, res)


if __name__ == '__main__':
    unittest.main()
